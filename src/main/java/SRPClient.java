import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class SRPClient {

    private static final int SALT_BIT_LENGTH = 16;
    private static final int BIT_LENGTH = 16;
    private static final int RADIX = 16;

    private MessageDigest messageDigest;
    private MessageDigest userDataDigest;
    private MessageDigest scramblerDigest;

    private BigInteger multiplier;
    private BigInteger generator;
    private BigInteger modulus;
    private BigInteger salt;

    private BigInteger privateKey;


    public SRPClient() {
       this.salt = new BigInteger(SALT_BIT_LENGTH, new Random()).abs();
       this.modulus = BigInteger.probablePrime(BIT_LENGTH, new SecureRandom()).abs();
       this.generator = BigInteger.valueOf(2);
       this.multiplier = BigInteger.valueOf(3);
       this.privateKey = new BigInteger(BIT_LENGTH, new Random());

       try {
           messageDigest = MessageDigest.getInstance("MD5");
           userDataDigest = MessageDigest.getInstance("MD5");
           scramblerDigest = MessageDigest.getInstance("MD5");
       } catch (NoSuchAlgorithmException e) {
           e.printStackTrace();
           System.exit(-1);
       }
    }

    public String getModulus() {
        return modulus.toString(RADIX);
    }

    public String getGenerator() {
        return generator.toString(RADIX);
    }

    public String getMultiplier() {
        return multiplier.toString(RADIX);
    }

    public String getSalt() {
        return salt.toString(RADIX);
    }

    public BigInteger getVerifier(String username, String password) {
        return generator.modPow(getSecret(username, password), modulus);
    }

    public String getScrambler(String A, String B) {
        scramblerDigest.update(A.getBytes());
        scramblerDigest.update(B.getBytes());

        return new BigInteger(scramblerDigest.digest()).toString(RADIX);
    }

    private BigInteger getSecret(String username, String password) {
        messageDigest.update(salt.toByteArray());
        userDataDigest.update((username + ":" + password).getBytes());
        messageDigest.update(userDataDigest.digest());

        return new BigInteger(messageDigest.digest());
    }

    public String getPublicKey() {
        return generator.modPow(privateKey, modulus).toString(RADIX);
    }

    public String getSessionKey(String username, String password, String A, String B) {
        BigInteger secret = getSecret(username, password);
        BigInteger scrambler = new BigInteger(getScrambler(A, B), RADIX);

        return new BigInteger(B, RADIX).subtract(multiplier.multiply(generator.modPow(secret, modulus))).modPow(privateKey.add(scrambler.multiply(secret)), modulus).toString(RADIX);
    }
}
