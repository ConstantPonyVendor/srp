import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SRPServer {

    private final int RADIX = 16;

    private BigInteger modulus;
    private BigInteger generator;
    private BigInteger multiplier;

    private MessageDigest scramblerDigest;

    private BigInteger privateKey;

    public SRPServer(String modulus, String multiplier, String generator) {
        this.privateKey = new BigInteger(16, new Random());

        this.modulus = new BigInteger(modulus, RADIX);
        this.generator = new BigInteger(generator, RADIX);
        this.multiplier = new BigInteger(multiplier, RADIX);

        try {
            scramblerDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String getScrambler(String A, String B) {
        scramblerDigest.update(A.getBytes());
        scramblerDigest.update(B.getBytes());

        return new BigInteger(scramblerDigest.digest()).toString(RADIX);
    }

    public String getPublicKey(BigInteger verifier) {
       return multiplier.multiply(verifier).add(generator.modPow(privateKey, modulus)).toString(RADIX);
    }

    public String getSessionKey(BigInteger verifier, String A, String B) {
        BigInteger scrambler =  new BigInteger(getScrambler(A, B), RADIX);

        return new BigInteger(A, RADIX).multiply(verifier.modPow(scrambler, modulus)).modPow(privateKey, modulus).toString(RADIX);
    }
}
