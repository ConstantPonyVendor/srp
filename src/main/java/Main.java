import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {

        SRPClient SRPClient = new SRPClient();
        System.out.println("SRPClient ready!");

        SRPServer SRPServer = new SRPServer(SRPClient.getModulus(), SRPClient.getMultiplier(), SRPClient.getGenerator());
        System.out.println("SRPServer is ready!");
        System.out.println();

        System.out.println("/* Parameters */");
        System.out.println("modulus: " + SRPClient.getModulus());
        System.out.println("generator: " + SRPClient.getGenerator());
        System.out.println("multiplier: " + SRPClient.getMultiplier());
        System.out.println();

        String username = "user";
        String password = "pass";

        System.out.println("/* Password data */");
        System.out.println("username: " + username);
        System.out.println("password: "+ password);
        System.out.println("salt: " + SRPClient.getSalt());

        BigInteger verifier = SRPClient.getVerifier(username, password);
        System.out.println("verifier: " + verifier);
        System.out.println();

        System.out.println("/* Auth */");
        String A = SRPClient.getPublicKey();
        String B = SRPServer.getPublicKey(verifier);

        System.out.println("\'A\' from SRPClient: " + A);
        System.out.println("\'B\' form SRPServer: " + B);
        System.out.println();

        System.out.println("Scrambler from SRPClient: " + SRPClient.getScrambler(A, B));
        System.out.println("Scrambler from SRPServer: " + SRPServer.getScrambler(A, B));
        System.out.println();

        System.out.println("Session key from SRPClient: " + SRPClient.getSessionKey(username, password, A, B));
        System.out.println("Session key from SRPServer: " + SRPServer.getSessionKey(verifier, A, B));
    }
}
